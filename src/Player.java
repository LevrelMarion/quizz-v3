import java.util.ArrayList;
import java.util.Scanner;

public class Player {
    private String userName;
    private int score = 0;

    ArrayList<String> userAnswers = new ArrayList<>();

    // Constructeur
    Player() {
        System.out.println("Quel est votre nom ?");
        Scanner sc = new Scanner(System.in); //lire l'entrée
        userName = sc.nextLine(); // stocker l'entrée
    }

    public String getUserName() {
        return this.userName;
    }

    public int getUpdateScore() {
        return this.score;
    }
}
