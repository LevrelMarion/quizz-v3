import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;
import static java.lang.System.lineSeparator;

public class Qcm extends Question {

    protected ArrayList choices;
    String rightAnswer;


    // Parametres de Qcm
    public Qcm(String questionParameter, ArrayList<String> answer, String rightAnswer) {
        //Constructeurs de la mere
        super(questionParameter, rightAnswer);
        choices = answer;
    }

    //overider pour que le getQuestion et tryAnswer ne changent pas dans le main
    @Override
    public String getQuestions() {
        //Pour afficher la liste de choix dans la proposition de la seconde question
        // présenter la liste sous forme de liste
        String afficheChoix = "";
        for (int i = 0; i < choices.size(); i++) {
            afficheChoix = afficheChoix + lineSeparator() + (i + ")" + choices.get(i));
        }
        return question + afficheChoix;
    }

    @Override
    public Boolean tryAnswer(String userAnswer) {
        if (userAnswer.toLowerCase().equals(answer.toLowerCase()) || (userAnswer.equals(Integer.toString(choices.indexOf(rightAnswer))))) {
            return true;
        }
        return false;
        //Lui donner la bonne réponse
    }
}


