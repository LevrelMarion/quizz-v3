import java.net.http.HttpHeaders;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.lineSeparator;

public class Main {
    public static void main(String[] args) {

        // Instancier (appeler) une class
        Player player = new Player();
        int score = 0;

        // créer liste pour indexer les réponses du QCM
        ArrayList<String> answer = new ArrayList<>();
        answer.add(0, "Rex");
        answer.add(1, "Milou");
        answer.add(2, "Rox");

        // instancier Questionnaire
        ArrayList<Question> questions = new ArrayList<>();
        Question myQuestion = new Question("Quel est la couleur du cheval blanc d'Henri IV ?", "Blanc");
        Question myQcm = new Qcm("Comment s'appelle le chien de Tintin ?", answer, "1");
        Question myVraiFaux = new VraiFaux("L'humain d'idéfix s'appelle Obélix", true);
        questions.add(myQuestion);
        questions.add(myQcm);
        questions.add(myVraiFaux);



        // appeler la liste de questions (
        for (Question q : questions) {
            System.out.println(q.getQuestions()); //afficher le premier paramètre dans la ArrayList
            //reponse user
            Scanner sc = new Scanner(System.in);
            String userAnswer = sc.nextLine();
            //aller chercher la methode pour essayer le resultat
            Boolean resultat = q.tryAnswer(userAnswer);
            if (resultat) {
                score += 1;
            }
        }
        Integer finalScore = player.getUpdateScore();
        if (finalScore < 3) {
            System.out.println("Ca n'est pas fameux, " + player.getUserName() + " votre score est " + score + "...");
        } else {
            System.out.println("Bravo, " + player.getUserName() + " ! Votre score est " + score + " .");

        }
    }
}
