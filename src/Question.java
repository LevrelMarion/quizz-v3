import java.util.List;

public class Question {

    // déclarer les variables
    protected String question;
    protected String answer;

    // constructeur
    public Question(String questionParameter, String answerParameter) {
        this.question = questionParameter;
        this.answer = answerParameter;
    }

     // Getter(accesseurs)
    public String getQuestions() {
        return this.question;
    }

    // Creer une methode pour if pour vérfier si les réponses sont égales aux réponses d'orignes dans la ArrayList (deuxième paramètre)
    public Boolean tryAnswer(String userAnswer) {
        if (userAnswer.toLowerCase().equals(answer.toLowerCase())) {
            return true;
        }
        return false;
    }
}

