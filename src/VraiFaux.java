public class VraiFaux extends Question {

    Boolean correctAnswer = null;
    Boolean userAnswerBoolean;

    public VraiFaux(String questionParameter, Boolean correctAswerParameter) {
        super(questionParameter, correctAswerParameter.toString());
        correctAnswer = correctAswerParameter;
    }

    @Override
    public String getQuestions() {
        //Pour afficher la liste de choix dans la proposition de la seconde question
        return "Vrai ou Faux ?" + System.lineSeparator() + question;
    }
// ajouter avec vrai - oui - true
        public Boolean compareAnswer(String userAnswer) {
        if ((userAnswer.toLowerCase().equals("vrai")) || (userAnswer.toLowerCase().equals("oui")) || (userAnswer.toLowerCase().equals("true")) || (userAnswer.toLowerCase().equals("si")))
            { return userAnswerBoolean = true;}
        else {
            return userAnswerBoolean = false;
        }
    }
    @Override
    public Boolean tryAnswer(String userAnswer) {
        this.compareAnswer(userAnswer);
        if(userAnswerBoolean.equals(correctAnswer)) {
            return true;
        } else {
            return false;
        }
    }
 }